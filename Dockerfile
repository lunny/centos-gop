
FROM lunny/centos-go
LABEL maintainer="xiaolunwen@gmail.com"
RUN	go get gitea.com/lunny/gop

ENV PATH /root/go/bin:$PATH