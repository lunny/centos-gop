# Usage

[![Build Status](https://drone.gitea.com/api/badges/lunny/centos-gop/status.svg)](https://drone.gitea.com/lunny/centos-gop)

```
docker run --rm -v "$PWD":/myrepo -w /myrepo lunny/centos-gop:latest gop build
```